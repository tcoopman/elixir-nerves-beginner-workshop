defmodule Fw2 do
  @moduledoc """
  Documentation for Fw2.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Fw2.hello
      :world

  """
  def hello do
    :world
  end
end
