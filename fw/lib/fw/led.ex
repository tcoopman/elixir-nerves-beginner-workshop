defmodule Fw.Led do
  require Logger
  alias ElixirALE.GPIO

  def start_link(led_pin) do
    GPIO.start_link(led_pin, :output)
  end

  @doc """
  Turns on the led on this `pid`
  """
  def on(pid) do
    GPIO.write(pid, 1)
  end

  @doc """
  Turns off the led on this `pid`
  """
  def off(pid) do
    GPIO.write(pid, 0)
  end

  @doc """
  Blinks the led on this `pid` for a fixed amount of `time` in ms.

  The `length` is the length of time in ms for one blink, so one full cycle of on and off.
  For example passing 200 to `length` will turn the led on for 100ms and then off for 100ms
  """
  def blink(pid, time, frequency) do
    number_of_cycles = div(time, frequency)
    sleep_length = div(frequency, 2)

    for _ <- 1..number_of_cycles do
      GPIO.write(pid, 1)
      Process.sleep(sleep_length)
      GPIO.write(pid, 0)
      Process.sleep(sleep_length)
    end
  end
end
