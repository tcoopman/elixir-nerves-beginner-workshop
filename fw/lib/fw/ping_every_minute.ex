defmodule Fw.PingEveryMinute do
  require Logger
  use GenServer

  alias ElixirALE.GPIO

  def start_link(led_pin) do
    GenServer.start_link(__MODULE__, led_pin)
  end

  @impl GenServer
  def init(led_pin) do
    Registry.register(Pomodoro.Dispatcher, :timer, [])
    {:ok, led_pid} = GPIO.start_link(led_pin, :output)
    {:ok, led_pid}
  end

  @impl GenServer
  def handle_info({:advance_time, 1}, led_pid) do
    Logger.info(fn -> "Advancing time, light led" end)
    GPIO.write(led_pid, 1)
    Process.sleep(100)
    GPIO.write(led_pid, 0)
    {:noreply, led_pid}
  end
end
