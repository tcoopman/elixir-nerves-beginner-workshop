defmodule Fw.LedOnOff2Buttons do
  require Logger
  use GenServer
  alias ElixirALE.GPIO

  def start_link([button_off_pin: _, button_on_pin: _, led_pin: _] = opts) do
    GenServer.start_link(__MODULE__, opts)
  end

  @impl GenServer
  def init(button_off_pin: button_off_pin, button_on_pin: button_on_pin, led_pin: led_pin) do
    {:ok, button_off_pid} = GPIO.start_link(button_off_pin, :input)
    {:ok, button_on_pid} = GPIO.start_link(button_on_pin, :input)
    GPIO.set_int(button_off_pid, :rising)
    GPIO.set_int(button_on_pid, :rising)
    {:ok, led_pid} = GPIO.start_link(led_pin, :output)
    {:ok, %{button_off_pin: button_off_pin, button_on_pin: button_on_pin, led_pid: led_pid}}
  end

  @impl GenServer
  def handle_info(
        {:gpio_interrupt, button_pin, :rising},
        %{led_pid: led_pid, button_off_pin: button_off_pin} = state
      )
      when button_pin == button_off_pin do
    Logger.info(fn ->
      "Writing 0 to #{inspect(led_pid)}"
    end)

    GPIO.write(led_pid, 0)
    {:noreply, state}
  end

  @impl GenServer
  def handle_info(
        {:gpio_interrupt, button_pin, :rising},
        %{led_pid: led_pid, button_on_pin: button_on_pin} = state
      )
      when button_pin == button_on_pin do
    Logger.info(fn ->
      "Writing 1 to #{inspect(led_pid)}"
    end)

    GPIO.write(led_pid, 1)
    {:noreply, state}
  end

  # even with listening to rising we still seem to get a falling, so we ignore these inputs for now
  def handle_info({:gpio_interrupt, pin, message}, state) do
    Logger.info(fn ->
      "Got a message #{message} for pin: #{pin}"
    end)

    {:noreply, state}
  end
end
