defmodule Fw.Pomodoro do
  require Logger
  use GenServer

  alias ElixirALE.GPIO
  alias Fw.Led

  def start_link([button_on_pin: _, button_off_pin: _, working_pin: _, pauzing_pin: _] = opts) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  @impl GenServer
  def init(
        button_on_pin: button_on_pin,
        button_off_pin: button_off_pin,
        working_pin: working_pin,
        pauzing_pin: pauzing_pin
      ) do
    # setup buttons
    {:ok, button_off_pid} = GPIO.start_link(button_off_pin, :input)
    {:ok, button_on_pid} = GPIO.start_link(button_on_pin, :input)
    GPIO.set_int(button_off_pid, :rising)
    GPIO.set_int(button_on_pid, :rising)

    # setup leds
    {:ok, working_led_pid} = Led.start_link(working_pin)
    {:ok, pauzing_led_pid} = Led.start_link(pauzing_pin)

    # listen to the pomodoro timer
    Registry.register(Pomodoro.Dispatcher, :listener, [])

    # return the state
    {:ok,
     %{
       button_off_pin: button_off_pin,
       button_on_pin: button_on_pin,
       working_led_pid: working_led_pid,
       pauzing_led_pid: pauzing_led_pid
     }}
  end

  @impl GenServer
  def handle_info(
        {:state_changed, :working},
        %{working_led_pid: working_led_pid, pauzing_led_pid: pauzing_led_pid} = state
      ) do
    Logger.info(fn -> "State changed: working" end)

    Led.on(working_led_pid)
    Led.off(pauzing_led_pid)

    {:noreply, state}
  end

  @impl GenServer
  def handle_info(
        {:state_changed, :pauzing},
        %{working_led_pid: working_led_pid, pauzing_led_pid: pauzing_led_pid} = state
      ) do
    Logger.info(fn -> "State changed: pauzing" end)

    Led.off(working_led_pid)
    Led.on(pauzing_led_pid)

    {:noreply, state}
  end

  @impl GenServer
  def handle_info(
        {:gpio_interrupt, pin, :rising},
        %{
          button_on_pin: button_on_pin,
          working_led_pid: working_led_pid,
          pauzing_led_pid: pauzing_led_pid
        } = state
      )
      when pin == button_on_pin do
    Logger.info(fn -> "Starting the pomodor timer" end)
    Pomodoro.start_link()

    for led_pid <- [working_led_pid, pauzing_led_pid] do
      Task.async(fn -> Led.blink(led_pid, 2_000, 200) end)
    end
    |> Task.yield_many()

    Led.on(working_led_pid)
    Led.off(pauzing_led_pid)

    {:noreply, state}
  end

  @impl GenServer
  def handle_info({:gpio_interrupt, pin, message}, state) do
    Logger.info(fn ->
      "Ignoring message #{message} for pin: #{pin}"
    end)

    {:noreply, state}
  end
end
