defmodule UiWeb.PomodoroController do
  use UiWeb, :controller

  def status(conn, _params) do
    json(conn, "ok")
  end
end