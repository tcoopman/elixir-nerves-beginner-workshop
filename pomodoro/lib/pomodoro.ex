defmodule Pomodoro do
  alias Pomodoro.Server

  def start_link() do
    GenServer.start_link(Server, nil)
  end

  def stop(pid) do
    :ok = GenServer.stop(pid)
  end

  def state(pid) do
    GenServer.call(pid, :state)
  end
end
