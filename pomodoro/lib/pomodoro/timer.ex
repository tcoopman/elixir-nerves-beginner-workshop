defmodule Pomodoro.Timer do
  use GenServer

  def start_link(length_of_minute) do
    GenServer.start_link(__MODULE__, length_of_minute, name: :timer)
  end

  @impl GenServer
  def init(length_of_minute) do
    start_timer(length_of_minute)
    {:ok, length_of_minute}
  end

  @impl GenServer
  def handle_info(:timeout, length_of_minute) do
    Registry.dispatch(Pomodoro.Dispatcher, :timer, fn listeners ->
      for {pid, _} <- listeners, do: send(pid, {:advance_time, 1})
    end)

    start_timer(length_of_minute)
    {:noreply, length_of_minute}
  end

  defp start_timer(length_of_minute), do: Process.send_after(self(), :timeout, length_of_minute)
end
