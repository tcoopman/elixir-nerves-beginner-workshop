defmodule Pomodoro.Application do
  use Application

  @length_of_minute Application.fetch_env!(:pomodoro, :length_of_minute)

  def start(_type, _args) do
    children = [
      {Registry, [keys: :duplicate, name: Pomodoro.Dispatcher]},
      {Pomodoro.Timer, @length_of_minute}
    ]

    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
