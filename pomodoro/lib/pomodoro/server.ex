defmodule Pomodoro.Server do
  use GenServer
  alias Pomodoro.Impl

  def init(_) do
    Registry.register(Pomodoro.Dispatcher, :timer, [])
    state = Impl.start()
    {:ok, state}
  end

  def handle_call(:state, _from, state) do
    reply = Impl.state(state)
    {:reply, reply, state}
  end

  def handle_info({:advance_time, time}, state) do
    new_state = Impl.advance_time(state, time)

    if not Impl.same_state(state, new_state) do
      Registry.dispatch(Pomodoro.Dispatcher, :listener, fn listeners ->
        for {pid, _} <- listeners do
          send(pid, {:state_changed, Impl.state(new_state)})
        end
      end)
    end

    {:noreply, new_state}
  end
end
