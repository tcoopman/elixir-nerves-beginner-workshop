defmodule Pomodoro.ImplTest do
  use ExUnit.Case
  alias Pomodoro.Impl

  test "start a pomodoro timer" do
    pomodoro = Impl.start()
    assert :working = Impl.state(pomodoro)
  end

  test "check the state after 24 minutes" do
    state =
      Impl.start()
      |> Impl.advance_time(24)
      |> Impl.state()

    assert :working = state
  end

  test "check the state after 25 minutes" do
    state =
      Impl.start()
      |> Impl.advance_time(25)
      |> Impl.state()

    assert :pauzing = state
  end

  test "pauzing timer after 5 minutes" do
    state =
      pauzing_pomodoro()
      |> Impl.advance_time(5)
      |> Impl.state()

    assert :working = state
  end

  test "advancing time over pauze in one step" do
    state =
      Impl.start()
      |> Impl.advance_time(31)
      |> Impl.state()

    assert :working = state
  end

  test "advancing time over multitple breaks in one step" do
    state =
      Impl.start()
      |> Impl.advance_time(25 + 5 + 25 + 1)
      |> Impl.state()

    assert :pauzing = state
  end

  defp pauzing_pomodoro, do: Impl.start() |> Impl.advance_time(25)
end
