defmodule Pomodoro.TimerTest do
  use ExUnit.Case, async: true

  alias Pomodoro.Timer

  setup do
    {:ok, _pid} = start_supervised({Registry, [keys: :duplicate, name: Pomodoro.Dispatcher]})
    :ok
  end

  test "Timer sends message every minute" do
    Registry.register(Pomodoro.Dispatcher, :timer, [])

    # fake minute length for tests
    minute = 10
    {:ok, pid} = Timer.start_link(minute)

    Process.sleep(45)
    assert_received({:advance_time, 1})
    assert_received({:advance_time, 1})
    assert_received({:advance_time, 1})
    assert_received({:advance_time, 1})

    GenServer.stop(pid)
  end
end
