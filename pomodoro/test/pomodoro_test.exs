defmodule PomodoroTest do
  use ExUnit.Case, async: false
  doctest Pomodoro

  defmodule TestTimer do
    use GenServer

    def start_link(stop_after) do
      GenServer.start_link(__MODULE__, stop_after)
    end

    def init(stop_after) do
      start_timer()
      {:ok, {0, stop_after}}
    end

    def handle_info(:timeout, {current, stop_after}) when current < stop_after do
      Registry.dispatch(Pomodoro.Dispatcher, :timer, fn listeners ->
        for {pid, _} <- listeners do
          send(pid, {:advance_time, 1})
        end
      end)

      start_timer()
      {:noreply, {current + 1, stop_after}}
    end

    def handle_info(:timeout, state), do: {:noreply, state}

    defp start_timer do
      Process.send_after(self(), :timeout, 1)
    end
  end

  setup do
    {:ok, _pid} = start_supervised({Registry, [keys: :duplicate, name: Pomodoro.Dispatcher]})

    {:ok, pomodoro} =
      start_supervised(%{
        id: Pomodoro,
        start: {Pomodoro, :start_link, []}
      })

    [pomodoro: pomodoro]
  end

  test "start a pomodoro timer", %{pomodoro: pomodoro} do
    assert :working = Pomodoro.state(pomodoro)
  end

  test "check the state after 24 minutes", %{pomodoro: pomodoro} do
    TestTimer.start_link(24)
    Process.sleep(50)

    assert :working = Pomodoro.state(pomodoro)
  end

  test "check the state after 25 minutes", %{pomodoro: pomodoro} do
    TestTimer.start_link(25)
    Process.sleep(60)

    assert :pauzing = Pomodoro.state(pomodoro)
  end

  test "pauzing timer after 5 minutes", %{pomodoro: pomodoro} do
    pauzing_pomodoro()
    TestTimer.start_link(5)
    Process.sleep(50)

    assert :working = Pomodoro.state(pomodoro)
  end

  test "stopping a pomodoro timer" do
    {:ok, pid} = Pomodoro.start_link()
    :ok = Pomodoro.stop(pid)

    assert false == Process.alive?(pid)
  end

  defp pauzing_pomodoro do
    TestTimer.start_link(25)
    Process.sleep(50)
  end
end
