# Install instructions up front

1. Follow the platform specific instructions
2. Follow the all platform instructions
3. Test the installation
4. Editor

Instructions from here and updated: https://hexdocs.pm/nerves/installation.html

## Mac specific

```bash
brew update
brew install fwup squashfs coreutils autoconf openssl wxmac
```

## Linux specific

### Ubuntu

```bash
sudo apt install build-essential automake autoconf git squashfs-tools ssh-askpass
```

### Archlinux

For archlinux `build-essential, automake, autoconf, git` will be ok probably.

`squashfs-tools` 

for `ssh-askpass` you can `pacman -Ss ssh-askpass` and choose one depending on your envioronment. After you've installed it, you'll probably need to set the `SSH_ASKPASS` variable to the location of the installed package. For example `SSH_ASKPASS=/usr/lib/ssh/ssh-askpass`

## All platforms

### Elixir and Erlang

The steps below install `elixir` and `erlang` with [ASDF](https://github.com/asdf-vm/asdf). This is not a requirement, you can install it with your own package manager, but make sure you have `erlang 20.3.x` and `elixir 1.6.x`

```bash
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.5.0


# The following steps are for bash. If you’re using something else, do the
# equivalent for your shell.
echo -e '\n. $HOME/.asdf/asdf.sh' >> ~/.bash_profile
echo -e '\n. $HOME/.asdf/completions/asdf.bash' >> ~/.bash_profile # optional
source ~/.bash_profile

asdf plugin-add erlang https://github.com/asdf-vm/asdf-erlang.git
asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir.git

# If on Debian or Ubuntu, you'll want to install wx before running the next
# sudo apt install libwxgtk3.0-dev
asdf install erlang 20.3.8
asdf install elixir 1.6.5
asdf global erlang 20.3.8
asdf global elixir 1.6.5
```

### Make sure you have the latest `hex` and `rebar`

```bash
mix local.hex
mix local.rebar
```

### Nerves

```bash
mix archive.install hex nerves_bootstrap
```

## Test installation

If you execute `iex` you should see something like this:

```bash
Erlang/OTP 20 [erts-9.3.3] [source] [64-bit] [smp:4:4] [ds:4:4:10] [async-threads:10] [hipe] [kernel-poll:false]

Interactive Elixir (1.6.5) - press Ctrl+C to exit (type h() ENTER for help)
iex(1)>
```

Pressing `Ctrl+c` twice exists. You're now done.

## Editor

To all have the same editor, let's use [Visual studio code](https://code.visualstudio.com/) and install the [ElixirLs](https://github.com/JakeBecker/vscode-elixir-ls) plugin